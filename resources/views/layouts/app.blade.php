<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ Lang::locale() }}" xml:lang="{{ Lang::locale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index">
    <meta name="description" content="{{ setting('site.description') }}">
    <meta name="keywords" content="{{ setting('site.keywords') }}">

    <meta property="og:title" content="@yield('og_title', setting('site.title'))">
    <meta property="og:description" content="@yield('og_description', setting('site.description'))">
    <meta property="og:image" content="@yield('og_image', setting('site.image'))">
    <meta property="fb:app_id" content="" />
    <meta property="og:url" content="{{ Request::fullUrl() }}">
    <meta property="og:site_name" content="{{ setting('site.title') }}">
    <meta property="og:type" content="website">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <link rel="shortcut icon" href="{{asset("images/icon/favicon.ico?v=1")}}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset("images/icon/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset("images/icon/apple-icon-60x60.png")}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset("images/icon/apple-icon-72x72.png")}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset("images/icon/apple-icon-76x76.png")}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset("images/icon/apple-icon-114x114.png")}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset("images/icon/apple-icon-120x120.png")}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset("images/icon/apple-icon-144x144.png")}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset("images/icon/apple-icon-152x152.png")}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset("images/icon/apple-icon-180x180.png")}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset("images/icon/android-icon-192x192.png")}}">
    <link rel="manifest" href="{{asset("images/icon//manifest.json")}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset("images/icon/ms-icon-144x144.png")}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=EB+Garamond:400|Karla:400,700,700i,400i|Kurale:400|Spirax:400">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <![endif]-->
    <title>@yield('page_title') {{ setting('site.title') }}</title>
  </head>
  <body ontouchstart="">
    @include('layouts.header')
    <div class="content">
      @yield('page_content')
    </div>
    @include('layouts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#c2a79e",
          "text": "#ffffff"
        },
        "button": {
          "background": "transparent",
          "text": "#ffffff",
          "border": "#ffffff"
        }
      },
      "showLink": false,
      "content": {
        "message": "",
        "dismiss": "Souhlasím"
      }
    })});
    </script>
    @yield('page_scripts')
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', '{{setting("site.google_analytics_tracking_id")}}', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{setting("site.google_analytics_tracking_id")}}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{setting("site.google_analytics_tracking_id")}}');
    </script>
  </body>
</html>
